# Avatar Legends RPG (Unofficial)

A FoundryVTT system for [Avatar Legends RPG](https://magpiegames.com/avatarrpg) characters.

<img src="https://i.imgur.com/w2mxjJW.png" alt="Player character sheet" width="60%" />

Screenshots: https://imgur.com/a/cA5Bfdg

## Installation
To install the system, paste the manifest URL below into the Install System dialog on the Setup menu:
  * `https://gitlab.com/pacosgrove1/legends/-/raw/main/system.json`

## Usage
See [the Wiki](https://gitlab.com/pacosgrove1/legends/-/wikis/home) for specific usage examples.

## Credits
This system exists thanks to the excellent YouTube tutorial series by [Cédric Hauteville](https://www.youtube.com/user/LieutenantRazak).

### Fonts
* [Bench Nine](https://www.fontsquirrel.com/fonts/benchnine) by Vernon Adams
* [Covered By Your Grace](https://fonts.google.com/specimen/Covered+By+Your+Grace?preview.text_type=alphabet&preview.size=37#standard-styles) by Kimberly Geswein
* [Hey August](https://www.behance.net/gallery/84310469/Free-Hey-August-Handwritten-Font) by Khurasan Studio
* [Komikazoom](https://www.1001fonts.com/komikazoom-font.html) by Apostrophic Labs
* [New Tegomin](https://fonts.google.com/specimen/New+Tegomin?preview.text_type=alphabet&preview.size=37&query=new+tegomin) by Kousuke Nagain
* [Village](https://www.dafont.com/prisoner.font) by Mark F. Heiman

_Licence details are included in the font folders._

### Images
* Icons from [Font Awesome](https://fontawesome.com/)
* Training images from [Game-icons.net](https://game-icons.net/)

### Disclaimer
Avatar: The Last Airbender, Nickelodeon, and all related marks, logos, and characters are owned by Viacom International Inc. This VTT system is not endorsed by, sponsored by, nor affiliated with Viacom International Inc., Magpie Games, or any other Avatar franchise, and is intended for private use only.
