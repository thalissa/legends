export const legends = {};

legends.stats = {
  "creativity": "legends.stats.creativity",
  "focus": "legends.stats.focus",
  "harmony": "legends.stats.harmony",
  "passion": "legends.stats.passion"
};

legends.approaches = {
  "defend-maneuver": "legends.techniques.approaches.defend-maneuver",
  "advance-attack": "legends.techniques.approaches.advance-attack",
  "evade-observe": "legends.techniques.approaches.evade-observe"
};

legends.training = {
  "water": "legends.training.water",
  "fire": "legends.training.fire",
  "earth": "legends.training.earth",
  "air": "legends.training.air",
  "weapons": "legends.training.weapons",
  "technology": "legends.training.technology"
}

legends.npcLevels = {
  "minor": "legends.actor-sheet.npc.levels.minor",
  "moderate": "legends.actor-sheet.npc.levels.moderate",
  "major": "legends.actor-sheet.npc.levels.major"
}

legends.defaultTokens = {
  'condition': 'systems/legends/images/tokens/condition.png',
  'moment-of-balance': 'systems/legends/images/tokens/moment-of-balance.png',
  'move': 'systems/legends/images/tokens/move.png',
  'npc': 'systems/legends/images/tokens/npc.png',
  'player': 'systems/legends/images/tokens/player.png',
  'technique': 'systems/legends/images/tokens/technique.png',
  'feature': 'systems/legends/images/tokens/feature.png',
  'npc-principle': 'systems/legends/images/tokens/npc-principle.png'
}
